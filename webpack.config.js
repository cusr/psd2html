const path = require('path');
const webpack = require('webpack');

module.exports = {
    context: path.join(__dirname, 'app'),
    entry:{
        app:'./app.js'
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, 'debug')
    },
    devtool: 'source-map',
    devServer: {
        contentBase: './debug',
        hot: true
    },
    plugins:[
        new webpack.ContextReplacementPlugin(/script$/),
		new webpack.NamedModulesPlugin(),
		new webpack.HotModuleReplacementPlugin()
    ],
    module: {
        rules:[
            {
                test: /\.css$/,
                use:[
                    {loader:'style-loader'},
                    {loader:'css-loader'},
                ]
            },
            {
                test: /\.less$/,
                use:[
                    {loader:'style-loader'},
                    {loader:'css-loader'},
                    {loader:'less-loader'},
                ]
            },
            {
                test: /\.json$/,
                use:[
                    {loader:'json-loader'},
                ]
            },
            {
                test: /\.pug$/,
                use:[
                    {loader:'pug-loader'},
                ]
            },
            {
                test: /\.png$/,
                use:[
                    {loader:'url-loader?name=img/[name]-[sha512:hash:base64:7].[ext]&limit=5000' },
                ]
            }
        ]
    }
};