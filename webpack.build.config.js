const config = require('./webpack.config');
const path = require('path');

config.devtool = 'source-map';
config.output = {
    path: path.join(__dirname, 'release'),
    filename: '[name].js'
};

module.exports = config;