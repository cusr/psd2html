import Widget from '../lib/Widget'
import * as d3 from '../lib/d3'
class DropControl extends Widget{
    constructor(pageControl){
        super(pageControl)
        this.root = d3.select(document.body)
        this._initDiv()
        this._initDropEvent()
        this.loadStyle('DropArea')
    }
    _initDiv(){
        this.dropArea = this.root.append('div')
        .attr('class', 'dropArea hidden')
        this.dropArea.append('div')
            .attr('class','drop-text')
            .text('Drop Here')
    }
    _initDropEvent(){
        let preventFunc = (event) => {
            event.stopPropagation()
            event.preventDefault()
            return false
        }
        this.root.on('dragover', ()=>{
            this.dropArea.classed('hidden', false)
            d3.event.preventDefault()
            return false
        })
        this.root.on('dragleave', ()=>{
            this.dropArea.classed('hidden', true)
        })
        this.root.on('drop', ()=>{
            this.dropArea.classed('hidden', true)
            d3.event.preventDefault()
            if(d3.event.dataTransfer.files.length > 0){
                let filename = d3.event.dataTransfer.files[0].path
                this.pageControl.page.loadPSD(filename)
            }
            return false
        })
    }
    reload(info){

    }
}

export default DropControl