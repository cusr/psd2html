import Page from "../lib/Page";
import * as d3 from "../lib/d3";
const {dialog} = window.require("electron").remote
const ipc = window.require('electron').ipcRenderer
const PSD = window.require("psd")

class MainPage extends Page{
    constructor(pageControl, info, data){
        super(pageControl, info, data);
    }
    enter(){
        super.enter();
    }
    loadPSD(filename){
        console.log(filename)
        var that = this
        PSD.fromURL(filename).then((psd)=>{
            let psdRoot = psd.tree()
            this.layerCounts = psdRoot.descendants().length
            this.layerNum = 0
            this.tree = {
                __pos:{left:0,top:0,x:0,y:0},
                __child:[],
                __group:[]
            }
            this.loadNode(psdRoot, this.root, this.tree)
            console.log(this.tree)
            this.root.selectAll("img")
                .on("mouseover",function(){
                    // if(this.__block) return;
                    let styles = this.getBoundingClientRect()
                    this.__block = that.addBlock(styles.width, styles.height, styles.left, styles.top)
                })
                .on("mouseout", function(){
                    this.__block.remove()
                    this.__block = null
                })
            d3.select(document.body).on("keydown",function(){
                if(d3.event.code === "KeyS"){
                    dialog.showOpenDialog({
                        properties:[
                            "openDirectory",
                            "createDirectory",
                            "promptToCreate",]
                        },
                        filename=>that.save(filename[0])
                    )
                }
            })
        })
    }
    outputPug(){
        let pugText = ""
        function _outputPug(node, level){
            node.__child.forEach(e=>{
                pugText += `${'\t'.repeat(level)}.${e.name}\n`
            })
            node.__group.forEach(e=>{
                pugText += `${'\t'.repeat(level)}.${e.name}\n`
                _outputPug(e, level + 1)
            })
        }
        _outputPug(this.tree, 0)
        console.log(pugText)
    }
    outputLess(folderPath){
        let lessText = ""
        function _outputPug(node, level){
            let tab0 = '\t'.repeat(level)
            let tab1 = '\t'.repeat(level+1)
            node.__child.forEach(e=>{
                lessText += 
`${tab0}.${e.name}{
${tab1}position: absolute;
${tab1}width: ${e.width}px;
${tab1}height: ${e.height}px;
${tab1}left: ${e.left}px;
${tab1}top: ${e.top}px;
${tab1}background-image: url('${folderPath}/${e.name}.png');
${tab1}background-repeat: none;
${tab0}}
`
            })
            node.__group.forEach(e=>{
                lessText += 
`${tab0}.${e.name}{
${tab1}position: absolute;
${tab1}left: ${e.left}px;
${tab1}top: ${e.top}px;
${tab1}width: ${e.width}px;
${tab1}height: ${e.height}px;
`
                _outputPug(e, level + 1)
                lessText += tab0 + "}\n"
            })
        }
        _outputPug(this.tree, 0, {left:0, top:0})
        console.log(lessText)
    }
    addBlock(width,height,x,y){
        return this.root.append("div")
            .attr("class","block")
            .style("width",width+"px")
            .style("height",height+"px")
            .style("left",x+"px")
            .style("top",y+"px")
    }
    removeBlock(){
        this.root.selectAll(".block")
            .remove()
    }
    loadNode(node, div, tree){
        if(node.hasChildren()){
            let children = node.children()
            children.forEach((child)=>{
                if(child.hasChildren()){
                    let groupAttrs = child.export()
                    let newDiv = div.append('group')
                    newDiv.attr("name", groupAttrs.name)
                    newDiv.style("display", groupAttrs.visible ? "block" : "none")
                    let descendants = child.descendants()
                    let left = d3.min(descendants, child=>child.hasChildren()? Infinity : child.get("left"))
                    let top = d3.min(descendants, child=>child.hasChildren()? Infinity : child.get("top"))
                    let width = d3.max(descendants, child=>child.hasChildren()? -Infinity : child.get("left")+child.get("width")) - left
                    let height = d3.max(descendants, child=>child.hasChildren()? -Infinity : child.get("top")+child.get("height")) - top
                    newDiv.style("left",left - tree.__pos.left + "px")
                    newDiv.style("top",top - tree.__pos.top + "px")
                    newDiv.style("width",width + "px")
                    newDiv.style("height",height + "px")
                    let newTree = {
                        __visible:groupAttrs.visible,
                        name: groupAttrs.name,
                        left: left,
                        top: top,
                        width: width,
                        height: height,
                        __pos:{left:left, top:top},
                        __group:[],
                        __child:[]
                    }
                    tree.__group.push(newTree)
                    this.loadNode(child, newDiv, newTree)
                }else if(child.layer.image){
                    var attrs = child.layer.export()
                    let img = child.layer.image.toPng()
                    img.style.width = `${attrs.width}px`
                    img.style.height = `${attrs.height}px`
                    img.style.left = `${attrs.left - tree.__pos.left}px`
                    img.style.top = `${attrs.top - tree.__pos.top}px`
                    img.className = attrs.name
                    img.style.display = attrs.visible ? 'block' : 'none'
                    img.style['z-index'] = this.layerCounts - this.layerNum++
                    console.log(img.style['z-index'])
                    div.node().appendChild(img)

                    tree.__child.push({
                        name:attrs.name,
                        width:attrs.width,
                        height:attrs.height,
                        left:attrs.left - tree.__pos.left,
                        top:attrs.top - tree.__pos.top,
                        visible:attrs.visible,
                        img:img
                    })
                }
            })
        }
    }
    save(folderpath){
        console.log(folderpath)
        if(folderpath){
            function convertImg(node){
                node.__child.forEach(e=>{
                    let canvas =document.createElement("canvas")
                    canvas.width = e.width, canvas.height = e.height
                    let ctx = canvas.getContext("2d")
                    ctx.drawImage(e.img, 0, 0)
                    let imageData = canvas.toDataURL("image/png")
                    e.imgBase64 = imageData.replace(/data:image\/png;base64,/,"");
                })
                node.__group.forEach(e=>{
                    convertImg(e)
                })
            }
            convertImg(this.tree)
            ipc.sendSync('save', folderpath, this.tree)
            this.outputPug()
            this.outputLess(folderpath)
        }
    }
}

export default MainPage;