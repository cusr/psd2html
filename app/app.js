import PageControl from "./lib/PageControl.js";
import DropControl from './script/DropControl'
import * as d3 from "./lib/d3";
import "./style/main.less"

window.onload = function(){
    let content = d3.select(document.body).append('div')
        .attr('class','content')
        .append('div')
        .attr('class','warpper')
        
    var pageControl = new PageControl({},content)
    
    let dropControl = new DropControl(pageControl)
    pageControl.addWidget('DropControl', dropControl)

    pageControl.loadPage("mainPage");
}