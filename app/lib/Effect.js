import * as d3 from "./d3";

function getOption(optionObj, name, defaultValue)
{
	return optionObj ? (optionObj[name] ? optionObj[name] : defaultValue) : defaultValue;
}

class Effect {
    constructor(options) {
        this.start = getOption(options, "start", { style: "opacity", value: 0 });
        this.end = getOption(options, "end", { style: "opacity", value: 1 });
        this.callback = getOption(options, "callback", function() {});
        this.duration = getOption(options, "duration", 300);
        this.delay = getOption(options, "delay", 0);
        this.easeType = getOption(options, "easeType", d3.easeCubicInOut);
    }
    reverse(){
        return new Effect({
            start: this.end,
            end: this.start,
            callback: this.callback,
            delay: this.delay,
            easeType: this.easeType
        });
    }
    static FadeIn() {
        return new Effect({
            start: { style: "opacity", value: 0 },
            end: { style: "opacity", value: 1 }
        });
	}
	static FadeOut(){
        return new Effect({
            start: { style: "opacity", value: 1 },
            end: { style: "opacity", value: 0 }
        });
    }
}

export default Effect;