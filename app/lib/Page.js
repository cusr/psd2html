//base class
class Page {
    constructor(pageControl, info, data) {
        this.pageControl = pageControl
        this.info = info
        this.root = pageControl.pageNode
        this.name = info.name
        this.data = data
        this._loadStyle()
        this._loadPage(this._loadPug())
    }
    _loadStyle(){
        let lessRequire = require.context("../style/", true, /^\.\/.*\.less$/)
        try{
            let filename = 'style' in this.info ? this.info.style : this.info.name
            lessRequire(`./${filename}.less`)
        }catch(e){
            console.error(e)
        }
    }
    _loadPug(){
        let pugRequire = require.context('../page/', true, /^\.\/.*\.pug$/)
        let pug = undefined
        try{
            let filename = 'pug' in this.info ? this.info.pug : this.info.name
            pug = pugRequire(`./${filename}.pug`)
        }catch(e){
            console.error(e)
        }
        return pug
    }
    _loadPage(pug) {
        if(pug !== undefined){
            this.pageControl.pageNode.html(pug())
        }
    }
    enter() {
        //header and footer event init
    }
    exit() {
        this.root.selectAll(".btn")
            .on("click", null)
    }
}

module.exports = Page
