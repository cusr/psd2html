import * as d3 from "./d3"

import Effect from "./Effect.js"

// const d3 = Object.assign({}, d3select, d3transition)

function load(path){
	//tell webpack only to parse js file in pageControl folder
	//else it will parse all the files from the root folder and child folder
	let req = require.context("../script/",true,/^\.\/.*.js$/)
	return  req("./"+path+".js").default
}

function fillOption(obj, propName, defaultValue){
    obj.hasOwnProperty(propName) ? '' : obj[propName] = defaultValue
}


function getEffect(optionEffect, infoEffect, defaultEffect){
    if(optionEffect !== undefined)
        return optionEffect
    else if(infoEffect !== undefined)
        return infoEffect
    return defaultEffect
}

class PageControl {
    constructor(pageInfos, root) {
        this.page = null
        this.root = root ? root : d3.select(window.document.body)
		this.pageNode = null
        this.enterEffect = null// Effect.FadeIn()
        this.exitEffect = null//Effect.FadeOut()
        this.pageHistory = []
        this.widgets = new Map()
        this.pageInfos = pageInfos
        this.pageInfo = null
    }
    addWidget(name, widget) {
        this.widgets.set(name, widget)
    }
    getWidget(name) {
        return this.widgets.get(name)
	}
	setDefaultEnterEffect(effect){
		this.enterEffect = effect
		return this
	}
	setDefaultExitEffect(effect){
		this.exitEffect = effect
		return this
	}
    loadLastPage() {
        if (this.pageHistory.length > 0) {
            this.pageHistory.pop()
            var lastPage = this.pageHistory.pop()
            this.loadPage(options)
        }
    }
    _parseOptions(args){
        let pageNameRegex = /(.+)\{(.+)\}/
        if(typeof args[0] === 'object'){
            fillOption(args[0], 'pageName', '')
            let className = args[0].pageName,
                uniqueKey = ''
            if(pageNameRegex.test(args[0].pageName)){
                let rst = pageNameRegex.exec(args[0].pageName)
                className = rst[1]
                uniqueKey = rst[2]
            }
            fillOption(args[0], 'className', className)
            fillOption(args[0], 'uniqueKey', uniqueKey)
            fillOption(args[0], 'data', null)
            fillOption(args[0], 'back', false)
            fillOption(args[0], 'enter', undefined)
            fillOption(args[0], 'exit', undefined)
            fillOption(args[0], 'front', false)
            return args[0]
        }else if(typeof args[0] === 'string'){
            let back = false,
                data = null,
                pageName = args[0],
                className = pageName,
                uniqueKey = ''
            if(pageNameRegex.test(pageName)){
                let rst = pageNameRegex.exec(pageName)
                className = rst[1], uniqueKey = rst[2]
            }
            if(args.length > 1){
                if(typeof args[1] === 'boolean'){
                    back = args[1]
                    if(args.length > 2 && args[2] === 'object')
                        data = args[2]
                }else if(typeof args[1] === 'object')
                    data = args[1]
            }
            return {
                pageName:pageName,
                className:className,
                uniqueKey:uniqueKey,
                data:data,
                back:back,
                enter:undefined,
                exit:undefined,
                front:false
            }
        }else{
            console.error('wrong parameta for loadPage')
        }
        return {}
    }
    /**
     * @param {string} pageName
     * @param {bool} reverse
     * @param {object} data
     * @example 
     *  loadPage('mainPage') // load mainPage
     *  loadPage('mainPage{startup}') // load mainPage with unique key
     *  loadPage('mainPage', true) // load mainPage using reverse animation
     *  loadPage('mainPage', data) // load mainPage with data
     *  loadPage('mainPage', true, data) // load mainPage using reverse animation
     *  loadPage({pageName:"",className:"",uniqueKey:"",data:{},back:false,effectIn:Effect,effectOut:Effect,front:flase}) // load page with options object
     * @memberof PageControl
     */
    loadPage(){
        this._loadPage(this._parseOptions(arguments))
    }
    preloadPage(){

    }
    _loadPage(options) 
    {
        let pageName = options.pageName,
            className = options.className,
            uniqueKey = options.uniqueKey,
            data = options.data,
            back = options.back,
            enterEffect = options.enter,
            exitEffect = options.exit,
            front = options.front,
            newPageInfo = null
        if (this.pageInfos.hasOwnProperty(className)) 
            newPageInfo = this.pageInfos[className]
        else
            newPageInfo = {
                name:className,
                script:className,
                pug:className,
                style:className
            }
        
        //old page exit
        let oldPage = this.page
        if (oldPage) {
            oldPage.exit()
        }
        
            exitEffect = getEffect(
                exitEffect,
                this.pageInfo ? this.pageInfo.exitEffect : undefined,
            this.exitEffect)

        enterEffect = getEffect(
            enterEffect,
            newPageInfo.enterEffect,
            this.enterEffect)
        if(back){
            [exitEffect, enterEffect] = [enterEffect, exitEffect]
            enterEffect = enterEffect ? enterEffect.reverse() : null
            exitEffect = exitEffect ? exitEffect.reverse() : null
        }
        //old page exit animation
        if(this.pageNode){
            if (exitEffect) {
                this.pageNode
                    .styles(exitEffect.start)
                    .transition()
                    .delay(exitEffect.delay)
                    .duration(exitEffect.duration)
                    .ease(exitEffect.easeType)
                    .styles(exitEffect.end)
                    .on("end", function(){
                        exitEffect.callback.call(this)
                    })
                    .remove()
            }
            else {
                //if there is no animation then just remove this page
                this.pageNode.remove()
            }
        }
        //create new content div
        //and replace current this.content
        if(this.pageNode)
            this.pageNode = this.root.insert("div", '.p-page')
        else
            this.pageNode = this.root.append("div")

        this.pageNode.attr("class", `p-page p-${className}` + (uniqueKey.length>0 ? ` p-${className}-${uniqueKey}` : ''))
            
        this.widgets.forEach((widget, name)=>{
            widget.reload(pageName, data)
        })

        //load new page resource
        let newPageClass = load(newPageInfo.script)
        //init new page
        let newPage = new newPageClass(this, newPageInfo, data)

        //excute new page animation
        if (enterEffect) {
            this.pageNode
                .styles(enterEffect.start)
                .transition()
                .delay(enterEffect.delay)
                .duration(enterEffect.duration)
                .ease(enterEffect.easeType)
                .styles(enterEffect.end)
                .on("end", function(){
                    newPage.enter()
                    enterEffect.callback.call(this)
                })
        }
        else 
        {
            newPage.enter()
        }
        //set current page and pageInfo to new page
        this.page = newPage
        this.pageInfo = newPageInfo
        //push current page to history
        this.pageHistory.push(options) // need clone data? 
    }
}

export default PageControl
