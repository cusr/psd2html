class Widget {
    constructor(pageControl, info) { 
        this.pageControl = pageControl
        this.info = info
    }
    loadStyle(filename){
        let lessRequire = require.context("../style/", true, /^\.\/.*\.less$/)
        try{
            lessRequire(`./${filename}.less`)
        }catch(e){
            console.error(e)
        }
    }
    loadPug(filename){
        let pugRequire = require.context('../page/', true, /^\.\/.*\.pug$/)
        let pug = undefined
        try{
            pug = pugRequire(`./${filename}.pug`)
        }catch(e){
            console.error(e)
        }
        return pug
    }
    reload(info) {
        //override me
    }
}

module.exports = Widget