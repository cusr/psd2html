const {app, ipcMain} = require('electron')
const fs = require("fs")
const path = require("path")

ipcMain.on('save', (event, folderpath, tree)=>{
    function saveImg(node){
        node.__child.forEach(e=>{
            let filename = path.join(folderpath, e.name+".png")
            fs.writeFileSync(filename, e.imgBase64, "base64")
        })
        node.__group.forEach(e=>{
            saveImg(e)
        })
    }
    saveImg(tree)
    event.returnValue = null
})