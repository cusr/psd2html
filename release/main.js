const {app, BrowserWindow} = require('electron')
const native = require('./native')
const process = require('process')

const useWebpackServer = (process.argv.indexOf('--server') !== -1)
let mainWindow = null

if(useWebpackServer)
    app.commandLine.appendSwitch('remote-debugging-port', '9222');
    
function createWindow()
{
    let options = {
        'title': 'psd2html'
    };
    if(useWebpackServer){
        options.webPreferences={
            webSecurity:false
        }
    }
    mainWindow = new BrowserWindow(options)
    if(useWebpackServer){
        mainWindow.loadURL('http://127.0.0.1:8080')
    }else{
        mainWindow.loadURL(`file://${__dirname}/index.html`)
    }
    if(process.argv.indexOf('--devTool'))
        mainWindow.webContents.openDevTools()
    mainWindow.focus()
    // Emitted when the window is closed.
    mainWindow.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })
}
app.on('ready', () => {
    createWindow()
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
})

app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin')
        app.quit()
})